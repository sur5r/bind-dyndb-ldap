Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bind-dyndb-ldap
Source: https://pagure.io/bind-dyndb-ldap

Files: *
Copyright: 2008-2014 Red Hat
License: GPL-2+

Files: src/acl.c
Copyright: 2001-2008 Internet Systems Consortium, Inc. ("ISC")
           2009 Red Hat
License: GPL-2+ and ISC

Files: src/krb5_helper.c
Copyright: 2009 Simo Sorce <ssorce@redhat.com>
License: GPL-2+

Files: debian/*
Copyright: 2012 Timo Aaltonen <tjaalton@debian.org>
License: GPL-2+

Files: debian/patches/workaround-missing-headers.patch
Copyright: 2000-2012 Internet Systems Consortium, Inc. ("ISC")
License: ISC

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
 REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS.  IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT,
 INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.
